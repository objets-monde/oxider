# Oxyder

Oxyder is a audio granulator plugin built for objets monde usign Camomile on MacOS

## Install

### clone this repo with submodules
```
git clone --recursive  git@gitlab.com:objets-monde/oxider.git

```
or init recursivly the

```
git submodule update --init --recursive
```


### Build camomille
```
cd lib/Camomile
git submodule update --init --recursive
mkdir build && cd build
cmake .. -G"Xcode"
cmake --build .
```

### make plugins
```
build_plugins.sh
```

